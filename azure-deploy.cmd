@ECHO ON

REM Install Hugo if it's not installed
pushd D:\home\site\deployments\tools
if not exist Hugo md Hugo
cd Hugo
if exist bin goto build
md bin
cd bin
:install
REM curl -o hugo0207.zip -L https://github.com/spf13/hugo/releases/download/v0.20.7/hugo_0.20.7_Windows-64bit.zip
curl -o hugo0556.zip -L https://github.com/gohugoio/hugo/releases/download/v0.55.6/hugo_0.55.6_Windows-64bit.zip
echo Installing Hugo...
SetLocal DisableDelayedExpansion & d:\7zip\7za x hugo0556.zip

REM Generate Hugo static site from source on GitHub
:build
popd
cd challenge2www
call D:\home\site\deployments\tools\hugo\bin\hugo -d D:\home\site\wwwroot
echo Hugo build finished successfully.
